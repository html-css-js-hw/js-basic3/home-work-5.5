//ТЗ 1
function divideNumbers (a, b) {
    if (b === 0) {
        console.log("Помилка: Ділення на нуль не допускається.");
        return undefined;
    }
    return a / b;
}
const result = divideNumbers(10, 2);
console.log("Результат ділення: " + result);

//ТЗ 2
function performUserOperation() {
    let num1, num2, operation;
    do {
        num1 = parseFloat(prompt('Введіть перше число:'));
    } while (isNaN(num1));
    do {
        num2 = parseFloat(prompt('Введіть друге число:'));
    } while (isNaN(num2));
    do {
        operation = prompt('Введіть математичну операцію (+, -, *, /):');
    } while (!['+', '-', '*', '/'].includes(operation));

    const result = performOperation(num1, num2, operation);
    if (result !== undefined) {
        console.log(`Результат ${num1} ${operation} ${num2} = ${result}`);
    }
}
function performOperation(num1, num2, operation) {
    switch (operation) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            if (num2 === 0) {
                alert('Ділення на нуль неможливе.');
                return undefined;
            }
            return num1 / num2;
        default:
            alert('Такої операції не існує.');
            return undefined;
    }
}
performUserOperation();

//ТЗ 3
function factorial(n) {
    if (n === 0 || n === 1){
        return 1;
    }
    return n * factorial(n - 1);
}
const num = 5;
const factResult = factorial(num);
console.log(`Факторіал числа ${num} = ${factResult}`);